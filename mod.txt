{
	"name": "XP Bank",
	"description": "Store XP while you're level 100 and then withdraw it when you Infamy",
	"author": "Undefined",
	"contact": "",
	"version": "1.2.5",
  "priority": 10,
  "blt_version": 2,
  "keybinds": [
		{
			"keybind_id": "xpbank_keybind_id",
			"name": "xpb_keybind_name",
			"description": "xpb_keybind_desc",
			"script_path": "lua/xpbank_keybind.lua",
			"run_in_menu": true,
			"run_in_game": false,
			"localized": true
		}
  ],
	"hooks": [
    {
      "hook_id": "lib/managers/experiencemanager",
      "script_path": "lua/xpbank_main.lua"
    },
    {
      "hook_id": "lib/managers/crimespreemanager",
      "script_path": "lua/xpbank_main.lua"
    }
	],
  "updates": [
    {
    "identifier": "XPBank",
    "host": {
        "meta": "https://bitbucket.org/pd2-rs/xp-bank/downloads/meta.json"
      }
    }
  ]
}